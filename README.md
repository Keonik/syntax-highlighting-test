# js
```js
var conf = prepareConfig(config);
```

# json
```json
{
    "maxIterations": 1,
    "sessionLabel": "Template",
    "tests": [
        {
            "path": "web",
            "name": "TEST_Template"
        }
    ]
}
```
# yaml
```YAML
description: my test
includes: myFunctions.js
actors:
  - actor: WEB
    segments:
      - segment: 1
        actions:
```

# python
```python
import os


def prepare_config(data : string):
    test = data
    return data
```
# bash
```bash
docker run -it --rm -name "Name" mycontainer:latest
# this is a comment
export myVar=value
echo "$myVar"
```